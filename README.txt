********************************************************************
                     D R U P A L    M O D U L E
********************************************************************
Name: Article module
Author: Nicholas Young-Soares
Email: drupal at codemonkeyx dot net
Last update: April 15, 2006
Drupal: 4.7

********************************************************************
DESCRIPTION:

The article module allows for articles (which are any kind of node)
to be organized and displayed in a centralized location.

********************************************************************
SYSTEM REQUIREMENTS:

Drupal: 4.7

********************************************************************
INSTALLATION:

see the INSTALL file in this directory.

********************************************************************
TODO:

- Add some more refined configuration options.
- Add better documentation.

********************************************************************
UPCOMING FEATURES:

- None planned

